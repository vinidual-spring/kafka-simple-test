package br.com.itau.caps.kafka.sender;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.caps.kafka.service.ReceiverCustomService;
import br.com.itau.caps.kafka.service.SenderCustomService;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SenderCustomTest {
	
	private enum map {
		KEY,
		VALUE
	};
	
	@Autowired
	private SenderCustomService senderCustom;
	
	@Autowired
	private ReceiverCustomService receiverCustom;

	@Test
	public void t01KafkaProduceCustom() {
		senderCustom.send(map.KEY.name(), map.VALUE.name());
	}
	
	@Test
	public void t02KafkaConsumerCustom() {
		receiverCustom.receive();
	}
	
}
