package br.com.itau.caps.kafka.consumer;

import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class KafkaConsumerCustom {
	
	private static Properties createProperties() {
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		return props;
	}
	
	private static Consumer<String, String> createConsumer() {
		return new KafkaConsumer<>(createProperties());
	}
	
	public static Consumer<String, String> getConsumer(){
		return createConsumer();
	}

}
