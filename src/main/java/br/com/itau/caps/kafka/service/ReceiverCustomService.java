package br.com.itau.caps.kafka.service;

import static java.util.Arrays.asList;

import java.time.Duration;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.itau.caps.kafka.consumer.KafkaConsumerCustom;

@Service
public class ReceiverCustomService {
	
	@Value("${kafka.topic}")
	private String topic;

	public void receive() {
		Consumer<String, String> consumer = KafkaConsumerCustom.getConsumer();
		consumer.subscribe(asList(topic));
		ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(10L));
		records.forEach(ReceiverCustomService::printRecord);
	}

	private static void printRecord(ConsumerRecord<String, String> record) {
		System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
	}

}
