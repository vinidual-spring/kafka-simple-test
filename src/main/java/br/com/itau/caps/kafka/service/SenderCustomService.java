package br.com.itau.caps.kafka.service;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.itau.caps.kafka.producer.KafkaProducerCustom;

@Service
public class SenderCustomService {
	
	@Value("${kafka.topic}")
	private String topic;

	private ProducerRecord<String, String> createRecord(String key, String value){
		return new ProducerRecord<String, String>(topic, key, value);
	}
	
	public void send(String key, String value) {
		Producer<String, String> producer = KafkaProducerCustom.getProducer();
		producer.send(createRecord(key, value));
	}
	
	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
